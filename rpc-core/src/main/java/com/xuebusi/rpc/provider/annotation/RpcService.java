package com.xuebusi.rpc.provider.annotation;

import java.lang.annotation.*;

/**
 * 服务提供者注解
 *
 * @author syj
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface RpcService {

    String version() default "";
}
