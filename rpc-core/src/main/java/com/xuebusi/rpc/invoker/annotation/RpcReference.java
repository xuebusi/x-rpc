package com.xuebusi.rpc.invoker.annotation;

import java.lang.annotation.*;

/**
 * 消费服务注解
 *
 * @author syj
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface RpcReference {

    String version() default "";

    String address() default "";
}
