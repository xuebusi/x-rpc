package com.xuebusi.rpc.invoker;

import com.xuebusi.rpc.net.params.RpcFutureResponse;
import com.xuebusi.rpc.net.params.RpcResponse;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * @author syj
 */
public class RpcInvokerFactory {

    private static volatile RpcInvokerFactory instance = new RpcInvokerFactory();

    public static RpcInvokerFactory getInstance() {
        return instance;
    }

    private ConcurrentMap<String, RpcFutureResponse> futureResponsePool = new ConcurrentHashMap<String, RpcFutureResponse>();

    public void setInvokerFuture(String requestId, RpcFutureResponse futureResponse) {
        futureResponsePool.put(requestId, futureResponse);
    }

    public void notifyInvokerFuture(String requestId, final RpcResponse response) {
        final RpcFutureResponse futureResponse = futureResponsePool.get(requestId);
        if (futureResponse == null) {
            return;
        }
        futureResponse.setResponse(response);
        futureResponsePool.remove(requestId);
    }
}
