#### 项目简介
    一个简易、高性能的RPC框架，使用Netty实现，Maven多模块项目，包含服务提供者、消费者、服务API、RPC框架共4个Maven子模块。

#### 模块简介
    rpc-core: 这是RPC核心框架部分，主要使用Netty实现;
    user-api: 这是服务API部分，用于Demo演示;
    user-provider: 这是服务提供者，用于Demo演示;
    user-consumer: 这是服务消费者，用于Demo演示;
    
#### 框架使用方式
    1.无论是服务提供者还是服务消费者，都需要依赖Spring环境(Spring容器);
    
    2.对于服务提供者，需要配置SpringRpcProviderFactory这个Bean，并指定要启动的TCP服务器的端口，
    然后在提供的服务实现类上加@RpcService注解，具体使用请参考 user-provider 这个子模块;
    
    3.对于服务的消费者，需要配置SpringRpcInvokerFactory这个Bean，并指定服务提供者的TCP服务器的IP和端口,
    然后在要消费的服务成员变量上加@RpcReference注解(不要使用Spring的Autowired注解)，
    具体使用请参考 user-consumer 这个子模块;
    
    4.先启动 user-provider，再启动 user-consumer，通过浏览器访问：
    http://localhost:18080/user/batchInvoke 接口，观察控制台日志输出.

#### 实现原理
    服务的提供者启动一个Netty服务器，服务的消费者启动一个Netty的客户端，
    Netty客户端将消费者要调用的服务名称和地址等信息发送到Netty服务端，
    Netty服务端解析请求，反射调用服务，将结果再响应给Netty客户端，完成服务调用。

#### 项目技术点
    1.Netty客户端和服务端编程;
    2.Spring扩展接口的应用;
    3.JDK动态代理的应用;
    4.反射的使用;
    5.Future的使用;
    6.Netty客户端连接管理;
    7.线程池的使用;

#### 待实现
    1.接入注册中心
    2.通信部分增加Mina和Nio实现