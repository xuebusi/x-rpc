package com.xuebusi.api;

import com.xuebusi.domain.User;

/**
 * 用户接口
 *
 * @author syj
 */
public interface IUserService {
    User getUserInfo(Integer id);
}
