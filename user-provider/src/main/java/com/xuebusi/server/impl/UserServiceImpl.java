package com.xuebusi.server.impl;

import com.xuebusi.api.IUserService;
import com.xuebusi.domain.User;
import com.xuebusi.rpc.provider.annotation.RpcService;
import org.springframework.stereotype.Service;

/**
 * 用户服务类
 *
 * @author syj
 */
@Service
@RpcService
public class UserServiceImpl implements IUserService {

    /**
     * 模拟用户数据
     *
     * @param id
     * @return
     */
    public User getUserInfo(Integer id) {
        User user = new User();
        user.setId(1);
        user.setUserName("苍井空");
        return user;
    }
}
