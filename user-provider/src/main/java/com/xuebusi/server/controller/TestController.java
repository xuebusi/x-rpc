package com.xuebusi.server.controller;

import com.xuebusi.rpc.provider.SpringRpcProviderFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 测试控制类
 *
 * @author syj
 */
@RestController
@RequestMapping
public class TestController {

    @GetMapping("/test")
    public String index() {
        return System.currentTimeMillis() + "";
    }

    @GetMapping("/test/getServices")
    public Map<String, Object> getServices() {
        SpringRpcProviderFactory factory = new SpringRpcProviderFactory();
        ConcurrentHashMap<String, Object> serviceDataMap = factory.getServiceDataMap();
        return serviceDataMap;
    }
}
