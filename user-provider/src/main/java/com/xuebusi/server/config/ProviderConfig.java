package com.xuebusi.server.config;

import com.xuebusi.rpc.provider.SpringRpcProviderFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 配置类
 *
 * @author syj
 */
@Configuration
public class ProviderConfig {

    @Value("${tcp.port}")
    private int port;

    @Bean
    public SpringRpcProviderFactory rpcProviderFactory() {
        SpringRpcProviderFactory factory = new SpringRpcProviderFactory();
        factory.setPort(port);
        return factory;
    }
}
