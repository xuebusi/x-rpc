package com.xuebusi.client.config;

import com.xuebusi.rpc.invoker.impl.SpringRpcInvokerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 配置类
 *
 * @author syj
 */
@Configuration
public class InvokerConfig {

    @Value("${service.provider.adress}")
    private String ip;

    @Value("${service.provider.port}")
    private int port;

    @Bean
    public SpringRpcInvokerFactory rpcInvokerFactory() {
        SpringRpcInvokerFactory factory = new SpringRpcInvokerFactory();
        factory.setIp(ip);
        factory.setPort(port);
        return factory;
    }
}
